import * as dotenv from 'dotenv';
import { IDatabaseConfig } from './interfaces/dbConfig.interface';

dotenv.config();

export const databaseConfig: IDatabaseConfig = {
  development: {
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME_DEVELOPMENT,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: process.env.DB_DIALECT,
  },
  test: {
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME_TEST,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: process.env.DB_DIALECT,
  },
  production: {
    username: 'edwvqhtfwoovqe',
    password: 'f39d46ccd4c1cba14caac23f2f0293e040f88b4d7e649ed89d53780f18c3fbda',
    database: 'daf8hdqv68jgkq',
    host: 'ec2-54-160-109-68.compute-1.amazonaws.com',
    dialect: 'postgres',
    dialectOptions: { 
      ssl: {
        require: true,
        rejectUnauthorized: false
    }
    }
  },
};
