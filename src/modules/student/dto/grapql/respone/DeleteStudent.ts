import { Field, InputType, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class DeleteStudentRes {
  @Field(() => String, { nullable: true })
  message: string;
}
