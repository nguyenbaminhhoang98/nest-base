import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class DeleteStudentInput {
  @Field(() => Int, { nullable: false })
  id: number;
}

//input là thứ truyền lên
