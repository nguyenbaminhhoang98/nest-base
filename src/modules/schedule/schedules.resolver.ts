import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import * as moment from 'moment';
import { CreateScheduleInput } from './dto/grapql/input/CreateShedule.input';
import { DeleteScheduleInput } from './dto/grapql/input/DeteleSchedule.input';
import {UpdateScheduleInput}  from './dto/grapql/input/UpdateSchedule.input';
import { DeleteScheduleType } from './dto/grapql/type/DeleteSchedule.types';
import { ScheduleType } from './dto/grapql/type/Schedule.type';
import { ScheduleService } from './schedules.service';

@Resolver()
export class ScheduleResolver {
  constructor(private scheduleService: ScheduleService) {}
  @Mutation(() => ScheduleType)
  async createSchedule(
    @Args('input') args: CreateScheduleInput
  ): Promise<ScheduleType> {
    console.log(args, 'hello');
    return await this.scheduleService.create(args);
  }

  @Query(() => [ScheduleType])
  async getSchedules(): Promise<ScheduleType[]> {


    const scheduleEnitities =await this.scheduleService.getAll();
    return scheduleEnitities.map((e) => {
      const startTime = moment(e.startTime, 'hh:mm:ss a');
      const endTime = moment(e.endTime, 'hh:mm:ss a');
      return  {
        ...e.toJSON(),
        learningTime: endTime.diff(startTime, 'hour'),
      }
     
    } )
    // return await this.scheduleService.getAll();
  }

  @Mutation (() => ScheduleType) 
  async updateSchedule( @Args('input') args:UpdateScheduleInput ): Promise<ScheduleType> {
    return await this.scheduleService.update(args.id,args.data);
  }



  @Mutation(() => DeleteScheduleType)
  async deteleSchedule(@Args('input') args: DeleteScheduleInput ): Promise<DeleteScheduleType> {
    try {
      const deleteRes = await this.scheduleService.delete(args.id);
      if(deleteRes != null || deleteRes > 0)
        return {id:-1}
      return {id: -2}
    } catch (err){
        console.log(err);
        return {id: 0}
    }
 
  }
}
