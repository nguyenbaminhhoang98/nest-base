import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class UpdateScheduleInputData {
  @Field(() => String, { nullable: false })
  note: string;
  @Field(() => Int, { nullable: false })
  teacherId: number;
  @Field(() => Int, { nullable: false })
  studentId: number;
  @Field(() => String, { nullable: false })
  startTime: string;
  @Field(() => String, { nullable: false })
  endTime: string;

}

@InputType()
export class UpdateScheduleInput {
  @Field(() => Int, { nullable: false })
  id: number;

  @Field(() => UpdateScheduleInputData)
  data: UpdateScheduleInputData;
}

//input là thứ truyền lên
