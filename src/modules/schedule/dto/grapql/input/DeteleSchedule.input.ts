import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class DeleteScheduleInput {
  @Field(() => Int, { nullable: false })
  id: number;


}

//input là thứ truyền lên
