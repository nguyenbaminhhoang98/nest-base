import { Field, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class DeleteScheduleType {
  @Field(() => Int, {nullable: true})
  id?:number
 
}

//type là chính là cái thứ trả về
