import { Field, ObjectType } from '@nestjs/graphql';
import { fileURLToPath } from 'url';
import { ScheduleType } from '../type/Schedule.type';

@ObjectType()
export class UpdateScheduleRespone {
  @Field(() => ScheduleType)
  data: ScheduleType;
}
