import { Field, ObjectType } from '@nestjs/graphql';
import { ScheduleType } from '../type/Schedule.type';

export interface ServerError {
  code: string;
  message: any;
}


@ObjectType()
export class DeleteScheduleRespone implements ServerError {
  @Field(()=> String)
  code: string;

  
  message: any;
}
